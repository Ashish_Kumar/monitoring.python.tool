import time
from . import settings
from django.utils.deprecation import MiddlewareMixin


class DjangoLoggingMiddleware(MiddlewareMixin):
    start = None
    db_connection = settings.DB_CONNECTION()

    def process_exception(self, request, exception):
        duration = time.time() - self.start

        self.db_connection.insert_db(request=request,
                                     exception=exception,
                                     duration=duration,
                                     type='exception')

    def process_request(self, request):
        self.start = time.time()

    def process_response(self, request, response):
        duration = time.time() - self.start
        if request.path_info.startswith(tuple(settings.IGNORED_PATHS)):
            return response

        if len(response.content) >= settings.MAX_RESPOSE_CONTENT_SIZE and response.status_code < 500:
            response.data = "Data exceeded the max content length."

        if response.status_code == 500:
            self.db_connection.insert_db(request=request,
                                         response=response,
                                         duration=duration,
                                         type='error')

        elif 400 <= response.status_code < 500:
            self.db_connection.insert_db(request=request,
                                         response=response,
                                         duration=duration,
                                         type='warning')

        else:
            self.db_connection.insert_db(request=request,
                                         response=response,
                                         duration=duration,
                                         type='success')

        return response
