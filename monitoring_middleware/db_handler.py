from pymongo import MongoClient
import datetime
import json
import uuid

from cassandra.cluster import Cluster, Session
from cassandra.query import SimpleStatement


class CassandraConnection(object):

    def __init__(self):

        self.keyspace_name = 'log_db'
        self.column_family_name = 'api'

        self.cluster = Cluster()
        self.session = self.cluster.connect()
        self.session.execute("CREATE KEYSPACE IF NOT EXISTS " + self.keyspace_name + " WITH REPLICATION ="
                                                                                     " {'class' : 'SimpleStrategy',"
                                                                                     " 'replication_factor' : 1 };")
        self.session.set_keyspace(self.keyspace_name)
        self.create_column_family()

    def create_column_family(self):
        self.session.execute("""
                            CREATE COLUMNFAMILY IF NOT EXISTS """ + self.column_family_name + """ (
                                id uuid,
                                request  text,
                                response text,
                                exception text,
                                duration float,
                                datetime timestamp,
                                api text,
                                type text,
                                method text,
                                PRIMARY KEY(id)
                            );
                        """)

    def insert_db(self,
                  request=None,
                  response=None,
                  duration=None,
                  type=None,
                  exception=None,
                  data=None):
        """
        :param request: django request object
        :param response: django response object
        :param duration: req-resp duration
        :param type: whether the request is 'success'|'warning'|'exception'|'error'
        :param exception: python exception object
        """

        insert_options = {
            'error': self.insert_error,
            'warning': self.insert_warning,
            'success': self.insert_success,
            'exception': self.insert_exception,
        }
        try:
            insert_meta = insert_options.get(type, None)(request=request,
                                                         response=response,
                                                         duration=duration,
                                                         table_name=self.column_family_name,
                                                         exception=exception,
                                                         )

            self.session.execute(insert_meta.get('query'), insert_meta.get('param'))
        except Exception as e:
            print('Unable to insert data Cassandra.Please Check\nERROR : {}'.format(e.__str__()))

        print("Cassandra-Row inserted successfully : of type :{}".format(type))

    @staticmethod
    def insert_error(request, response, duration, table_name, exception=None):

        api = request.path
        method = request.method.__str__()
        request = {
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            }
        response = {
                "status_code": response.status_code,
                "data": response.content.decode('utf-8'),
                "status_text": response.reason_phrase
            }
        d_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')

        query = SimpleStatement(
            "INSERT INTO " + table_name + " (id,request,response,duration,method,datetime,type,api) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
        )

        return {
            'query': query,
            'param': (uuid.uuid1(), str(request), str(response), duration, method, d_time, 'error', api)
        }

    @staticmethod
    def insert_warning(request, response, duration, table_name, exception=None):

        api = request.path
        method = request.method.__str__()
        response = {
                "status_code": response.status_code,
                "data": response.data,
                "status_text": response.status_text
            }
        request = {
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            }
        d_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')

        query = SimpleStatement(
            "INSERT INTO " + table_name + " (id,request,response,duration,method,datetime,type,api) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
        )

        return {
            'query': query,
            'param': (uuid.uuid1(), str(request), str(response), duration, method, d_time, 'warning', api)
        }

    @staticmethod
    def insert_success(request, response, duration, table_name, exception=None):

        api = request.path
        method = request.method.__str__()
        request = {
                "content_type": str(request.content_type),
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            }
        response = {
                "status_code": str(response.status_code),
                "data": response.data,
                "status_text": str(response.status_text)
            }

        d_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')

        query = SimpleStatement(
            "INSERT INTO " + table_name + " (id,request,response, method,duration,datetime,type,api) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
        )

        return {'query': query,
                'param': (uuid.uuid1(), str(request), str(response), method, duration, d_time, 'success', api)
                }

    @staticmethod
    def insert_exception(request, exception, duration, table_name, response):

        api = request.path
        method = request.method.__str__()
        request = {
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            }
        d_time = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
        exception = exception.__str__()

        query = SimpleStatement(
            "INSERT INTO " + table_name + " (id,request,exception,method,duration,datetime,type,api) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
        )

        return {
            'query': query,
            'param': (uuid.uuid1(), str(request), exception, method, duration, d_time, 'exception', api)
        }


class MongoConnection(object):

    def __init__(self):

        self.db_name = 'log_db'

        self.client = MongoClient("mongodb://localhost")
        self.db = None

        try:
            db_list = self.client.list_database_names()
            assert "log_db" in db_list
            self.db = self.client[self.db_name]
        except AssertionError:
            print("New database is created.")
            self.db = self.client[self.db_name]

    def insert_db(self,
                  request=None,
                  response=None,
                  duration=None,
                  type=None,
                  exception=None,
                  data=None):

        insert_options = {
            'error': self.error_log_format,
            'warning': self.warning_log_format,
            'success': self.success_log_format,
            'exception': self.exception_log_format,
        }
        try:
            insert_meta = insert_options.get(type, None)(request=request,
                                                         response=response,
                                                         duration=duration,
                                                         exception=exception,
                                                         )

            self.db.API.insert_one(insert_meta)
        except Exception as e:
            print('Unable to insert data Mongo.Please Check\nERROR : {}'.format(e.__str__()))

        print("Mongo-Document inserted successfully: of type: {}".format(type))

    @staticmethod
    def success_log_format(request,
                   response,
                   exception=None,
                   duration=None,):

        # TODO could provide a way to change their date-format too.

        object_format = {
            "type":"success",
            "request": {
                "api": request.path,
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            },
            "response": {
                "status_code": response.status_code,
                "data": response.data,
                "status_text": response.status_text
            },
            "datetime": datetime.datetime.now(),
            "duration": duration,
            "method": request.method
        }
        return object_format

    @staticmethod
    def error_log_format(request,
                           response,
                           exception=None,
                           duration=None, ):

        # TODO could provide a way to change their date-format too.

        object_format = {
            "type":"error",
            "request": {
                "api": request.path,
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            },
            "response": {
                "status_code": response.status_code,
                "data": response.content.decode('utf-8'),
                "status_text": response.reason_phrase
            },
            "datetime": datetime.datetime.now(),
            "duration": duration,
            "method": request.method
        }
        return object_format

    @staticmethod
    def exception_log_format(request,
                             response,
                           exception=None,
                           duration=None, ):

        # TODO could provide a way to change their date-format too.

        object_format = {
            "type":"exception",
            "request": {
                "api": request.path,
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            },
            "exception": exception.__str__(),
            "datetime": datetime.datetime.now(),
            "duration": duration,
            "method": request.method
        }
        return object_format

    @staticmethod
    def warning_log_format(request,
                           response,
                             exception=None,
                             duration=None, ):

        # TODO could provide a way to change their date-format too.

        object_format = {
            "type": "warning",
            "request": {
                "api": request.path,
                "content_type": request.content_type,
                "body": request.body.decode('utf-8') if len(request.body.decode('utf-8')) == 0
                else json.loads(request.body.decode('utf-8'))
            },
            "response": {
                "status_code": response.status_code,
                "data": response.data,
                "status_text": response.status_text
            },
            "datetime": datetime.datetime.now(),
            "duration": duration,
            "method": request.method
        }
        return object_format







