
import datetime
from cassandra.cluster import Cluster, Session
from cassandra.query import SimpleStatement, BatchStatement
from monitoring_middleware.db_handler import CassandraConnection


def search_by_date(column_family_name):

    from_date = input("Enter From Date \'YYYY-MM-DD'\': ")
    from_date = datetime.datetime.strptime(from_date, '%Y-%m-%d')

    to_date = input("Enter To Date \'YYYY-MM-DD'\': ")
    to_date = datetime.datetime.strptime(to_date, '%Y-%m-%d')

    query = SimpleStatement(
        "SELECT * FROM {} where datetime >= \'{}\' and datetime < \'{}\' ALLOW FILTERING;".format(column_family_name,
                                                                                                  from_date,
                                                                                                  to_date)
    )

    resp_dict = list()
    for cassandra_row in session.execute(query):
        resp_dict.append(cassandra_row)

    return resp_dict


def full_text_search(column_family_name):
    return ["COMING SOON"]


def top_n_requests(column_family_name):
    limit_value = int(input("\nEnter the int:n-value :"))
    resp_dict = list()

    query = SimpleStatement(
        "SELECT * from {} limit {};".format(column_family_name, limit_value)
    )

    for cassandra_row in session.execute(query):
        resp_dict.append(cassandra_row)

    return resp_dict


def request_type(column_family_name):
    flag = True
    valid_options = ['error', 'warning', 'exception', 'success']

    print("Valid Options: {}".format(valid_options))
    while flag:
        method = input("\nEnter the request type :")
        if method.lower() in valid_options:
            flag = False
        else:
            print("Invalid Choice.Valid Options: {}".format(valid_options))

    resp_dict = list()

    query = SimpleStatement(
        "SELECT * from {} where type=\'{}\';".format(column_family_name, method)
    )

    for cassandra_row in session.execute(query):
        resp_dict.append(cassandra_row)

    return resp_dict


def request_method(column_family_name):

    flag = True
    valid_options = ['GET', 'POST', 'DELETE', 'PATCH']

    print("Valid Options: {}".format(valid_options))
    while flag:
        method = input("\nEnter the method type :")
        if method.upper() in valid_options:
            flag = False
        else:
            print("Invalid Choice.\nValid Options: {}".format(valid_options))

    query = SimpleStatement(
        "SELECT * FROM {} where method=\'{}\';".format(column_family_name, method.upper())
    )

    resp_dict = list()
    for cassandra_row in session.execute(query):
        resp_dict.append(cassandra_row)

    return resp_dict


def menu():
    option = {
        1: (request_method, "Request Method Search"),
        2: (top_n_requests, "Top N-Search"),
        3: (full_text_search, "Full Text Search"),
        4: (request_type, "Request Type Search"),
        5: (search_by_date, "Search By Date-Range")
    }
    return option


def create_indexes(column_family_name):

    # TODO Using execute_async() may produce some race condition so currently .execute() is used.

    # batch = BatchStatement()
    SASI_index_fields = ['response', 'request']
    basic_index_fields = ['method', 'type']

    for field_name in basic_index_fields:
        query = SimpleStatement("CREATE INDEX IF NOT EXISTS on {} ({});".format(column_family_name, field_name))
        session.execute_async(query)

    for field_name in SASI_index_fields:
        query = SimpleStatement(
            """
             CREATE CUSTOM INDEX IF NOT EXISTS ON """ + column_family_name + """ (""" + field_name + """) USING 'org.apache.cassandra.index.sasi.SASIIndex'
             WITH OPTIONS = { 'mode': 'CONTAINS',
                              'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.StandardAnalyzer',
                              'analyzed': 'true', 'tokenization_skip_stop_words': 'and, the, or',
                              'tokenization_enable_stemming': 'true',
                              'tokenization_normalize_lowercase': 'true',
                              'tokenization_locale': 'en' }
            """
        )
        session.execute_async(query)

    # session.execute(batch)
    print("Index are created successfully")

    """
    CREATE CUSTOM INDEX IF NOT EXISTS idx_response ON logs (response) USING 'org.apache.cassandra.index.sasi.SASIIndex'
        WITH OPTIONS = { 'mode': 'CONTAINS',
                         'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.StandardAnalyzer',
                         'analyzed': 'true', 'tokenization_skip_stop_words': 'and, the, or',
                         'tokenization_enable_stemming': 'true',
                         'tokenization_normalize_lowercase': 'true',
                         'tokenization_locale': 'en' }  ;

    CREATE CUSTOM INDEX IF NOT EXISTS idx_request ON logs (request) USING 'org.apache.cassandra.index.sasi.SASIIndex'
        WITH OPTIONS = { 'mode': 'CONTAINS',
                        'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.StandardAnalyzer',
                        'analyzed': 'true', 'tokenization_skip_stop_words': 'and, the, or',
                        'tokenization_enable_stemming': 'true',
                        'tokenization_normalize_lowercase': 'true',
                        'tokenization_locale': 'en' }  ;
                        
    CREATE CUSTOM INDEX IF NOT EXISTS idx_date ON logs (request) USING 'org.apache.cassandra.index.sasi.SASIIndex'
        WITH OPTIONS = { 'mode': 'CONTAINS',
                        'analyzer_class': 'org.apache.cassandra.index.sasi.analyzer.NonTokenizingAnalyzer',
                        'analyzed': 'true' }  ;

    CREATE INDEX IF NOT EXISTS on logs (method);
    CREATE INDEX IF NOT EXISTS on logs (type);

    """


def run():

    keyspace_name = CassandraConnection().keyspace_name
    column_family_name = CassandraConnection().column_family_name

    global session

    cluster = Cluster()
    session = cluster.connect()
    query = SimpleStatement(
        "select keyspace_name from system_schema.tables where keyspace_name=%s"
    )
    result_keyspace = session.execute(query, (keyspace_name,))

    query = SimpleStatement(
        "select keyspace_name from system_schema.tables where keyspace_name=%s and table_name=%s"
    )
    result_columnfamily = session.execute(query, (keyspace_name, column_family_name))

    try:
        assert len(result_keyspace.current_rows) != 0
        assert len(result_columnfamily.current_rows) != 0
        session.set_keyspace(keyspace_name)
    except AssertionError:
        raise AssertionError("Invalid keyspace name or column_family name.")

    create_indexes(column_family_name)

    menu_option = menu()

    for key, value in menu_option.items():
        print("{}. {}".format(key, value[1]))

    option = int(input("Choose : "))
    if option in [key for key, value in menu_option.items()]:
        data = menu_option.get(option)[0](column_family_name)

        print(data)
        print(len(data))
