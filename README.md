# Monitoring Tool

### Table of content
<!--ts-->
   * [Table of content](#table-of-content)
   * [Pre-requisites](#pre-requisites)
   * [Installing Pre-requisites](#installing-pre-requisites)
   * [Project Setup](#project-setup)
   * [Cassandra Architecture Details](#cassandra-architecture-details)
   * [Cassandra Column-Family Fields](#cassandra-column-family-fields)
   * [MongoDB Document Fields](#mongodb-document-fields)
   * [Types Of Logs](#types-of-logs)
   * [Default Settings](#default-settings)
   * [Log Search Scripts](#log-search-scripts)
        * [Cassandra](#cassandra)
        * [MongoDB](#mongodb)
   * [Current Allowed Search's](#current-allowed-search's)
<!--te-->

### Pre-requisites
* Cassandra or MongoDB
* Python 3.x
* Django Project
* django-extensions

### Installing Pre-requisites
* Cassandra:
	`<link>` : <https://www.liquidweb.com/kb/install-cassandra-ubuntu-16-04-lts/>
* MongoDB:
	`<link>` : <https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04>
    
* django-extensions
	`<link>` : <https://django-extensions.readthedocs.io/en/latest/index.html>

### Project Setup
1.	Add **'monitoring_middleware.middleware.DjangoLoggingMiddleware'** in settings.py

```
    MIDDLEWARE = [
		......
        ......
        'monitoring_middleware.middleware.DjangoLoggingMiddleware'
    ]
```
2.	Copy both the folders in you django-webapp.

3. Setting for the database:
	* *Cassandra*:
		*	Add the code below in settings.py:
		```python
            MONITOR_INFO = {
                    "DB": {'db_type': 'cassandra',
                           'keyspace': 'log_db',
                           'table': 'api'
                           }
                }
        ```
	* *MongoDB*:
		* 	Add the code below in settings.py:
		    *Default Collection Name : '**API**'*
        ```python
                MONITOR_INFO = {
                        "DB": {'db_type': 'mongo',
                               'db_name': 'log_db',
                               'port': None,
                               }
                    }
         ```
        
----

### Cassandra Architecture Details
* Keyspace Creation settings:
```sh
CREATE KEYSPACE IF NOT EXISTS <keyspace_name> WITH REPLICATION = {'class' : 'SimpleStrategy','replication_factor' : 1 };
```
#### Cassandra Column-Family Fields

| Field Name | Data Type |
| ------ | ------ |
| id (PK)     | uuid |
| response | text |
| request | text |
| exception | text |
| duration | float |
| datetime | timestamp |
| api | text |
| type | text:'error'/'success'/'exception'/'warning' |
| method | text:POST/GET/DELETE/PATCH/PUT |

#### MongoDB Document Fields

| Field Name | Type |
| ------ | ------ |
| request     | {api:'',content_type:'',body:''} |
| response | {status_code:'',status_text:'',data:''} |
| datetime | datetime obj |
| duration | float |
| type | 'error'/'success'/'exception'/'warning' |
| method | POST/GET/DELETE/PATCH/PUT |
| exception | string |

#### Types Of Logs

* **SUCCESS** :  status code - *200*
* **WARNING** : status code - *400<= x <500*
* **EXCEPTION** : during and exception
* **ERROR** : status code - *500*

#### Default Settings
```python
    {           
            'DISABLE_EXISTING_LOGGERS':True,
            'IGNORED_PATHS':['/admin', '/static', '/favicon.ico'],
            'LOGGING_FIELDS':('status', 'reason', 'charset', 'headers', 'content'),
            'CONTENT_JSON_ONLY':True,
            'CONTENT_TYPES':None,
            'ENCODING':'utf-8',
            'ROTATE_MB':100,
            'ROTATE_COUNT':10,
            'MAX_RESPOSE_CONTENT_SIZE':3000,       # it is in bytes
    }
```

### Log Search Scripts
#### Cassandra
```sh
python manange.py runscript cassandra_log_search
```
#### MongoDB
```sh
python manange.py runscript mongo_log_search
```
### Current Allowed Search's
* Top n results
* Search by 'method'
* Search by 'type'
* Search by date range
* Full text search : [*under development*]




